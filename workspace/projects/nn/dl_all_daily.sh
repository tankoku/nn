#!/bin/bash

pushd /home/pi/workspace/bitbucket/nn/workspace/projects/nn

year_all=( 2018 )
# IFS=$'\n' tokyo_stock_all=($(cat codes.txt))
IFS=$'\n' tokyo_stock_all=($(cat 225-list.txt))

nm1="stocks_"
nm2="-T_1d_"

#curl -X POST -H 'Content-Type:application/x-www-form-urlencoded' -H 'User-Agent:Mozilla/5.0 (Windows NT 6.1; W…) ecko/20100101 Firefox/62.0' -d 'code=1301' -d 'year=2017' -d 'csv=' -o stocks_1301-T_1d_2017.csv -L https://kabuoji3.com/stock/file.php --insecure

#sed -i '1d' stocks_1301-T_1d_2017.csv

pushd stocks

for i in $(seq ${#tokyo_stock_all[*]})
# for code in ${tokyo_stock_all[@]}
do
    code=${tokyo_stock_all[$i-1]}
    if [ -z $code ]; then
        break
    fi
    if [ ! -d $code ]; then
        mkdir $code
    fi
    pushd $code

    for year in ${year_all[@]}
    do
        csv=$nm1$code$nm2$year".csv"
        curl -X POST -H 'Content-Type:application/x-www-form-urlencoded' -H 'User-Agent:Mozilla/5.0 (Windows NT 6.1; W…) ecko/20100101 Firefox/62.0' -d "code=$code" -d "year=$year" -d 'csv=' -o $csv -L https://kabuoji3.com/stock/file.php --insecure
        # echo $?
        sed -i '1d' $csv
        sleep 1
    done

    popd
done

find . -name "*.csv" -type f -empty -delete

popd

popd
