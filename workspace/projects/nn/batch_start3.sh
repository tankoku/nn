#!/bin/bash

folder="stocks/"
item="item.json"
nn="nn.json"
csv="nn.csv"

pushd ~/workspace/bitbucket/nn/workspace/projects/nn

pre=($(date "+%Y%m%d-%H%M%S-"))

touch $pre$nn
echo "[]" > $pre$nn

IFS=$'\n' elementes=($(cat 225-list.txt))
# elementes=($(/usr/bin/php  select_sym_nikkei225.php))
# pushd ${folder}
# elementes=($(ls -d *))
# popd

nmbr_of_elements=${#elementes[@]}
# echo ${nmbr_of_elements}
# perform every element
for (( i = 0 ; i < nmbr_of_elements ; i++ ))
do
   code=${elementes[$i]}
   sym=${folder}${code}

   # Secure new output folder
   OUT_DIR=$sym/out
   NEW_DIR=`dirname $OUT_DIR`
   [ ! -d $NEW_DIR ] && mkdir -p $NEW_DIR
   mkdir -p $OUT_DIR

#  touch $sym/out/${item}
#  echo "[{" > $sym/out/${item}

#  python3 readCsv3.py $sym

#  python3 writeCsv3.py $sym > $sym/out/new.txt
   rm -rf /tmp/iris_model/

#  python3 tfCsv3.py $sym

   pushd iris
   cp -f ../$sym/out/iris_test.csv ../$sym/out/iris_training.csv ../$sym/out/input.csv  .
   if [[ $# -gt 0 ]]; then
      env MPLBACKEND=Agg python3 pqs3.py $code train
      # backup model
      cp ../$sym/out/$code.npz /media/hp/bak/npz/$pre$code.npz
   else
      env MPLBACKEND=Agg python3 pqs3.py $code
   fi

#  echo "}]" >> ../$sym/out/${item}

   popd

   python3 nn3.py $sym $pre

   # real-time update, for 1st time
   # cp $nn  $pre$nn
   # sudo cp $nn  $csv /var/www/html/nn/
done

# pre=($(date "+%Y%m%d-%H%M%S-"))

cp -p $pre$nn   $nn
cp -p $pre$csv  $csv
mv    $pre$nn   /media/hp/bak/json/
mv    $pre$csv  /media/hp/bak/csv/
# cp $nn  $csv  /var/www/html/nn/
# scp -i ~pi/.ssh/dlbuy.pem nn.json nn.csv ubuntu@dlbuy.dip.jp:/var/www/html/ifis
scp -i ~pi/.ssh/raspi4.rsa.key nn.json nn.csv pi@10.48.6.167:/var/www/html/ifis

# rm  backup.zip
# find stocks/ -type d -name "out" -exec rm -rf {} \;

# zip backup.zip stocks -r
# cp -f backup.zip /var/www/html/nn/

popd

