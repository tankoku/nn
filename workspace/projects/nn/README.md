upgraded to python3.6

### 缘起
借鉴 iris 开花判断花种类 model ，依据单支股票日线价格时序列，进行未来价格趋势的预测。

https://github.com/t126tank/zigui2/tree/master/tf/sante

### win10 + py3.6 环境相关

```
> py -3 -m pip install beautifulsoup4
> py -3 -m pip install tensorflow
> py -3 -m pip install --upgrade tensorflow
> py -3 -m pip install tensorlayer
> py -3 -m pip install lxml
> py -3 -m pip install pandas
```

"TensorLayer does not support Tensorflow version older than 2.0.0.\n"
RuntimeError: TensorLayer does not support Tensorflow version older than 2.0.0.
Please update Tensorflow with:
 \- `pip install --upgrade tensorflow`
 \- `pip install --upgrade tensorflow-gpu`
https://github.com/tensorlayer/openpose-plus/issues/203#issuecomment-507225606

```
> py -3 -m pip install --upgrade tensorflow==1.12
> py -3 -m pip install --upgrade tensorlayer==1.11.0
```

### 目的
* RT 下获取日经225指数的成分株价格，按构成权重对未来指数的涨跌趋势进行预测


### 手法
* 使用 gym 库
* DQN

