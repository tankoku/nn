
<body>
  <form name="myForm" action="./index.php" method="post" >
  <table>

<?php
$dir    = './png/';
$commitJson = "commit.json";

if (isset($_POST['hidden'])){
  $commitArr = array();

  // read in all marked names
  $jsonStr = file_get_contents($commitJson);
  $markedArr = json_decode($jsonStr, true);
  // print_r($markedArr);

  // new
  foreach ($_POST as $rkey => $rval) {
    if ($rkey !== "hidden") {
      $radio['name']  = str_replace("_png", ".png", $rkey);
      $radio['trend'] = $rval === "zero"? 0: 1;
      $commitArr[] = $radio;
    }
  }
  print_r($commitArr);

  // old
  foreach ($markedArr as $marked)
    $commitArr[] = $marked;

  $fp = fopen($commitJson, 'w');
  fwrite($fp, json_encode($commitArr));
  fclose($fp);
}

// get all pngs from png/
$pngs = array_diff(scandir($dir), array('.', '..', 'memo.txt'));

// read in all marked names
$jsonStr = file_get_contents($commitJson);
$markedArr = json_decode($jsonStr, true);
// print_r($markedArr);

$markedNames = array_map(
  function ($obj) {
    return $obj['name'];
  }, $markedArr
);

// get difference from all pngs
$notMarkedNames = array_diff($pngs, $markedNames);
// print_r($notMarkedNames);

// twenty random elements each time
$notMarkedCnt = count($notMarkedNames);
if ($notMarkedCnt <= 0) {
  echo "</br><b>NOP NOP NOP</b></br>";
  exit(0);
}

$rand = $notMarkedCnt > 20? 20: $notMarkedCnt;
$not20 = array_rand($notMarkedNames, $rand);

foreach ($not20 as $pngIdx) {
  $pngfile = $dir . $notMarkedNames[$pngIdx];

  echo "<tr><td>";
  echo "<img src='$pngfile'></img></td>";
  // print($pngfile);

  echo <<<RADIOS
    <td>
RADIOS;

  echo "<input type='radio' name='$notMarkedNames[$pngIdx]' value='zero' checked>UNCERTAIN
        <input type='radio' name='$notMarkedNames[$pngIdx]' value='one'>LOW";

  echo <<<RADIOE
    </td>
  </tr>
RADIOE;

}

?>
    <tr>
      <td>
        <input type="submit" value="post">
      </td>
      <td>
        <input type="hidden" name="hidden" value="valStr"/>
        <input type="reset" value="reset">
      </td>
    </tr>
  </table>
  </form>
</body>
