#!/usr/bin/python3

from util.rd_conf import load_conf
from pathlib import Path

import sys
sys.path.append(Path('.'))

import pandas as pd
import os, glob


def o_f(op):
    if not os.path.exists(op):
        os.makedirs(op)

    os.chdir(op)


def ma_f(row, d, m):
    r = 0
    e = row.name  # end index
    # print(e)
    s = e + 1 - m

    if s >= 0:
        r = d.loc[s:e, 'price'].mean()

    return round(r, 2)


def diff_f(row, d, m):
    r = 0
    s = row.name  # current row index
    # print(s)

    if s >= m - 1:
        r = d.loc[s, 'lMa'] - d.loc[s, 'sMa']

    return round(r, 2)


def trend_f(row, d, m, f):
    r = 0
    s = row.name  # start index
    # print(s)

    if s >= m:
        diff1 = d.loc[s - 1, 'diffMa']
        diff2 = d.loc[s, 'diffMa']

        r = diff2 - diff1
        if diff1 * diff2 == 0:
            r = f * r
        elif diff1 * diff2 > 0:
            r = my_sign_with_abs(diff2) * abs(r)

    return round(r, 2)


def my_sign_with_abs(x):
    return 0.0 if abs(x) == 0 else x / abs(x)


def main(argv):
    conf = load_conf('./conf/pqsConfig.json')
    sMa = conf['shortMa']
    lMa = conf['longMa']
    factor = conf['factor']
    seqN = conf['seqN']
    ratio = conf['ratio']

    dataDir = "./data/"
    # csvs = "*20[1-2][8|9|0|1].csv"
    csvs = "japan-225-futures_*.csv"

    if len(argv) != 0:
        dataDir = argv[0]
        if len(argv) > 1:
            csvs = "japan-225-futures_*.csv.csv"

    frame = pd.DataFrame()
    list_ = []

    # Specify original data saved location/path
    os.chdir(dataDir)

    # Fetch all *.csv files
    for csvFile in glob.glob(csvs):
        # print csvFile

        # Maybe specify encode and read csv contents "SHIFT-JIS"
        df = pd.read_csv(csvFile, encoding="UTF-8", header=None, names=['time', 'price'])
        # print df

        df = df[df.columns[:2]]
        df.dropna(how='all')

        # df=df.rename(columns = {'two':'new_name'})
        # df.columns.values[0] = "time"
        # df.columns.values[1] = "price"

        list_.append(df)

    # Concat
    frame = pd.concat(list_)

    # count rows whether data is enough
    if lMa + seqN + 1 > frame.shape[0]:
        exit()

    # Remove odd rows
    # frame = frame[frame.volume > 0] # [frame.volume != 0]

    col_name = frame.columns[0]
    # print "[0]: ", col_name
    # print "Dir: ", srcDir
    # frame = frame.rename(columns = {col_name: 'tradeTime'})
    # print frame.tradeTime

    # Drop duplicated
    frame.drop_duplicates(subset=[col_name], inplace=True)

    # Sort
    # frame[col_name] = pd.to_datetime(frame.time)
    # frame.sort('tradeTime') This now sorts in date order (deprecated)
    frame.sort_values(by=[col_name], ascending=[True], inplace=True)  # from ver 0.17
    # frame[col_name] = frame[col_name].dt.strftime('%Y-%m-%d')

    # Reset idx : several files have index 0
    frame = frame.reset_index(drop=True)

    # Calculate trend value
    frame['sMa'] = frame.apply(ma_f, args=(frame, sMa), axis=1)
    frame['lMa'] = frame.apply(ma_f, args=(frame, lMa), axis=1)
    frame['diffMa'] = frame.apply(diff_f, args=(frame, lMa), axis=1)
    frame['trend'] = frame.apply(trend_f, args=(frame, lMa, factor), axis=1)

    # frame['tradeValue'] = frame['volume'] * ((frame['h']+frame['l']+2*frame['c'])//4)
    # frame['volume'] = frame['volume'] * ((frame['h']+frame['l']+2*frame['c'])//4) // frame['mp']

    # Output datasets
    o_f("../gym_trading/datasets")

    # Write all data
    frame.to_csv('japan-225-futures-trend.csv', header=False, index=False)

    trnsz = int(round((frame.shape[0] - lMa) * ratio))
    trnsz = (frame.shape[0] - lMa) - 20 - 1

    # Create train datasets
    frame.loc[lMa:lMa + trnsz, ['time', 'trend', 'price']].to_csv('japan-225-futures-trend-trn.csv', header=False,
                                                                  index=False)
    # Create test datasets
    frame.loc[lMa + trnsz + 1:, ['time', 'trend', 'price']].to_csv('japan-225-futures-trend-tst.csv', header=False,
                                                                   index=False)


if __name__ == "__main__":
    main(sys.argv[1:])

# Ref:
## http://pandas.pydata.org/pandas-docs/stable/genindex.html
## http://stackoverflow.com/questions/33642673/convert-csv-to-json-in-specific-format-using-python
## http://www.nephridium-labs.com/blog/converting-between-json-and-csv-using-pandas/
## https://github.com/nephridium/csv2json/blob/master/csv2json.py

# $ pip install -U pandas --upgrade --proxy=http://id:pw@proxy.global.net:8080

## * windows
# > py -3 -m pip install --upgrade pip
# > py -3 -m pip install pandas
