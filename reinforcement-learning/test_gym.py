import gym

from stable_baselines import PPO2

env = gym.make('CartPole-v1')

model = PPO2('MlpPolicy', env)
model.learn(total_timesteps=10000)

obs = env.reset()
for i in range(1000):
    action, _states = model.predict(obs, deterministic=True)
    obs, reward, done, info = env.step(action)
    env.render()
    if done:
        obs = env.reset()

env.close()
