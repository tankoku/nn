# https://ailog.site/2020/05/02/0502/

import gym
import matplotlib.pyplot as plt
import os
from env.Trend_env_train import TrendEnvTrade
from stable_baselines.common import set_global_seeds

from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import PPO2
from stable_baselines.bench import Monitor

# ログフォルダの作成
log_dir = './logs/'
os.makedirs(log_dir, exist_ok=True)


def train_PPO(env_train, model_name, timesteps=50000):
    """PPO model"""

    start = time.time()
    model = PPO2('MlpPolicy', env_train, ent_coef = 0.005, nminibatches = 8)
    #model = PPO2('MlpPolicy', env_train, ent_coef = 0.005)

    model.learn(total_timesteps=timesteps)
    end = time.time()

    model.save(f"{config.TRAINED_MODEL_DIR}/{model_name}")
    print('Training time (PPO): ', (end - start) / 60, ' minutes')
    return model


# 環境を生成（frame_boundはデータセット内の訓練範囲を開始行数と終了行数で指定）
env = gym.make('jp225-trend', frame_bound=(50, 100), window_size=10)
env = Monitor(env, log_dir, allow_early_resets=True)

# シードの指定
env.seed(0)
set_global_seeds(0)

# ベクトル化環境の生成
env = DummyVecEnv([lambda: env])

# モデルの生成
model = PPO2('MlpPolicy', env, verbose=1)

# モデルの読み込み(学習済みデータがある場合)
# model = PPO2.load('trading_model')

# モデルの学習
model.learn(total_timesteps=128000)

# モデルの保存
model.save('trading_model')

# モデルのテスト
env = gym.make('forex-v0', frame_bound=(50, 100), window_size=10)
env.seed(0)
state = env.reset()
while True:
    # 行動の取得
    action, _ = model.predict(state)
    # 1ステップ実行
    state, reward, done, info = env.step(action)
    # エピソード完了
    if done:
        print('info:', info)
        break

# グラフのプロット
plt.cla()
env.render_all()
plt.show()
