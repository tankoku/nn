# https://taiga.jpbiz.work:4083/project/tan-nn/us/8
from enum import Enum


class ActionSpace(Enum):
    S4 = -4
    S3 = -3
    S2 = -2
    S1 = -1
    DONOT = 0
    B1 = 1
    B2 = 2
    B3 = 3
    B4 = 4


class ActionStep(Enum):
    S = -1
    B = 1
