# https://taiga.jpbiz.work:4083/project/tan-nn/us/11
from enum import Enum


class PositionType(Enum):
    SHORT = -1
    LONG = 1
    EMPTY = 0

    '''
    def opposite(self):
        return Positions.Short if self == Positions.Long else Positions.Long
    '''