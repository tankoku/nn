# https://taiga.jpbiz.work:4083/project/tan-nn/us/11


from util.utils import action2num
from util.utils import datetime_fmt2ts
from entity.position_type import PositionType
from entity.action_space import ActionSpace


class PositionNode:
    def __init__(self, date, tm, price, action):
        self.__timestamp = None
        self.__date = date
        self.__time = tm
        if self.__timestamp is None:
            # 2021.04.21 4:57
            self.__timestamp = datetime_fmt2ts(self.__date, self.__time)

        self.__position_type = PositionType.LONG
        if action < ActionSpace.DONOT.value:
            self.__position_type = PositionType.SHORT
        self.__price = price
        self.__num = action2num(action)

    @property
    def date(self):
        return self.__date

    @property
    def time(self):
        return self.__time

    @property
    def position_type(self):
        return self.__position_type

    @property
    def price(self):
        return self.__price

    @property
    def num(self):
        return self.__num

    @property
    def timestamp(self):
        return self.__timestamp

    @num.setter
    def num(self, num):
        self.__num = num
