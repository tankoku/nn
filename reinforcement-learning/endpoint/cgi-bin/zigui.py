#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

# https://taiga.jpbiz.work:4083/project/tan-nn/us/13

import json, os, sys
import predict

from util import const

const.ERR_CD = -1

# print('Content-Type: text/html\n')

'''
print("sys.executable: ", sys.executable)
print("executable_link: ", os.readlink(sys.executable))
print("sys.path: ", sys.path)
print("sys.version_info: ", sys.version_info)
print("sys.api_version: ", sys.api_version)
'''

ret_act = 0  # DON'T
t = 0  # trend


def resp():
    print('Content-Type: application/json\n')
    act_obj = {"act": ret_act, "trend": t}
    print(json.dumps(act_obj))


if os.environ['CONTENT_LENGTH']:  # not None
    con = int(os.environ["CONTENT_LENGTH"])
    # print(con)

    market_json = sys.stdin.read(con)
    market_obj = json.loads(market_json)

    # debug
    # print('Content-Type:application/json\n\n')
    # print(json.dumps(market5min_json))

    trend = predict.mk_metadata(market_obj)
    if not trend:
        predict.inform({"mk_metadata": "failed"})
        resp()
        exit(const.ERR_CD)

    t = round(trend, 2)
    ret_act = predict.predict()
    if not ret_act:
        ret_act = 0
        predict.inform({"predict": "failed"})
        resp()
        exit(const.ERR_CD)

    market_obj.pop("last5min")
    market_obj["trend"] = trend
    market_obj["action"] = ret_act

    # trend in +/- 2 sigma
    if abs(trend) > 7.75 * 2:
        predict.inform(market_obj)
else:
    predict.inform({"access": "illegal"})

resp()
