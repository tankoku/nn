from .utils import load_dataset as _load_dataset


# Load JP225 5min datasets
JP225_5M = _load_dataset('test', 'timestamp')
JP225_5M_CGI = _load_dataset('cgi-test', 'timestamp')