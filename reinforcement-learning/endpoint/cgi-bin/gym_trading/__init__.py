
from gym.envs.registration import register
from copy import deepcopy

from . import datasets

register(
    id='jp2251-5m-v0',
    entry_point='gym_trading.envs:TrendEnvTrain',
    kwargs={
        'df': deepcopy(datasets.JP225_5M),
        'window_size': 2000,
        'frame_bound': (3000, len(datasets.JP225_5M)),
        'max_len': 10,
        'spread': 6
    }
)

register(
    id='cgi-jp2251-5m-v0',
    entry_point='gym_trading.envs:TrendEnvTrain',
    kwargs={
        'df': deepcopy(datasets.JP225_5M_CGI),
        'window_size': 2000,
        'frame_bound': (3000, len(datasets.JP225_5M_CGI)),
        'max_len': 10,
        'spread': 6
    }
)
