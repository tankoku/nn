# https://taiga.jpbiz.work:4083/project/tan-nn/us/14

import numpy as np
from .Trade_env import TradeEnv
from entity.action_space import *
from entity.position_node import PositionNode
from util.utils import *


class TrendEnvTrain(TradeEnv):

    def __init__(self, df, window_size, frame_bound, max_len=10, spread=6):
        assert len(frame_bound) == 2

        self.frame_bound = frame_bound
        super().__init__(df, window_size, max_len, spread)

    def _process_data(self):
        timestamps = self.df.loc[:, 'timestamp'].to_numpy()
        trends = self.df.loc[:, 'trend'].to_numpy()
        prices = self.df.loc[:, 'ask'].to_numpy()  # use ask and bid = ask - spread

        timestamps[self.frame_bound[0] - self.window_size]  # validate index (TODO: Improve validation)
        timestamps = timestamps[self.frame_bound[0] - self.window_size:self.frame_bound[1]]

        prices[self.frame_bound[0] - self.window_size]  # validate index (TODO: Improve validation)
        prices = prices[self.frame_bound[0] - self.window_size:self.frame_bound[1]]

        trends[self.frame_bound[0] - self.window_size]  # validate index (TODO: Improve validation)
        trends = trends[self.frame_bound[0] - self.window_size:self.frame_bound[1]]

        signal_features = np.column_stack((prices, trends))

        '''
        diff = np.insert(np.diff(prices), 0, 0)
        signal_features = np.column_stack((prices, diff))
        '''
        return timestamps, prices, signal_features

    def _calculate_reward00(self, action):
        new_action = action.item() - math.floor(len(ActionSpace) / 2)
        ts = self.timestamps[self._current_tick]
        d = ts2date(ts)
        t = ts2time(ts)
        ask = self.prices[self._current_tick]  # ask price
        p = ask

        if new_action == ActionSpace.DONOT.value:
            return 0., False

        if new_action < ActionSpace.DONOT.value:  # sell
            p -= self._spread  # bid price = ask - spread
        node = PositionNode(d, t, p, new_action)

        cd, _, _, pl = self._stack.stack_trade(node, ask)
        if cd == 1:
            self._pl_lst.append({
                "ts": ts,
                "pl": pl
            })
            # reward = cal_sharpe_ratio(self._pl_lst, ts, 8)
        return pl, cd == 1

    ##
    #@brief Calculates reward for every trading
    #@details pl is profit/loss
    #当前: 当open仓位接近上限时，调节act的幅度；达到上线后，“被迫”donot（持仓）
    #@todo: 对即时仓位相对上限作sigmoid,仓位较低时对reward 正反馈，反之负反馈
    #
    #@param self self
    #@param action buy/sell/donot
    #@return 类夏普率评估
    #
    #@note bar
    def _calculate_reward(self, action):
        reward = 0.
        new_action = action.item() - math.floor(len(ActionSpace) / 2)
        ts = self.timestamps[self._current_tick]
        d = ts2date(ts)
        t = ts2time(ts)
        ask = self.prices[self._current_tick]  # ask price
        p = ask

        if new_action < ActionSpace.DONOT.value:  # sell
            p -= self._spread  # bid price = ask - spread
        node = PositionNode(d, t, p, new_action)

        cd = 0
        space = None
        pl = 0
        while new_action != ActionSpace.DONOT.value:
            cd, space, _, pl = self._stack.stack_trade(node, ask)

            # Limited be Stack max_len, buy/sell may be not performed
            if cd < 0:  # space not enough and reduce order size
                step = ActionStep.B.value  # 4 -> 3 -> 2 ...
                if new_action < 0:
                    step = ActionStep.S.value  # -4 -> -3 -> -2 ...
                new_action -= step
                node = PositionNode(d, t, p, new_action)
            else:
                break

        # update profit-loss
        if new_action != ActionSpace.DONOT.value and cd == 1:  # close occurs
            self._pl_lst.append({
                "ts": ts,
                "pl": pl
            })
            reward = cal_sharpe_ratio(self._pl_lst, ts, 8)
            # self._stack.stack_print(ask)
            # print(' '.join([d, t]) + " | " + ' | '.join(list(map(str, [cd, space, pl]))))

        # step_reward
        return reward, cd == 1
        # return pl, cd == 1

