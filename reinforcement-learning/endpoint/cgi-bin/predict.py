# https://taiga.jpbiz.work:4083/project/tan-nn/us/16

import os
import random
import pandas as pd
import cgi_mk_datasets
import cgi_run_DRL

from util.utils import post_json
from util import const

const.MAIL_URL = 'ip_publisher'
const.JP2251_CSV = './cgi-bin/data/cgi-latest.csv'
const.JP2251_SHORT_CSV = './cgi-bin/data/cgi-short.csv'


def mk_metadata(m):
    ret = None
    # print(os.getcwd())

    if not os.path.exists(const.JP2251_CSV):
        return None

    try:
        with open(const.JP2251_CSV, 'a') as fd:
            # date, time (hh:mm), o, h, l, c, vol
            # remove duplications and empty lines in mk_datasets.py
            list(map(fd.write,
                     list(map(lambda x: ','.join(x) + "\n", list(map(lambda x: list(map(str, x)), m["last5min"]))))))

        # tail 20000
        pd.read_csv(const.JP2251_CSV, parse_dates=True).iloc[-20000:].reset_index(drop=True).to_csv(const.JP2251_SHORT_CSV, header=False, index=False)

        ''' delete empty lines from old to new
        with open(const.JP2251_CSV, 'r', encoding='utf-8') as inFile, \
                open(const.JP2251_SHORT_CSV, 'w', encoding='utf-8') as outFile:
            for line in inFile:
                if line.strip():
                    outFile.write(line)
        '''

        ret = cgi_mk_datasets.do()
    except Exception as e:
        # print(e)
        return None

    return ret


def predict():
    ret = None
    ret = random.randrange(-4, 4, 1)  # to use model to predict action code and return
    ret = cgi_run_DRL.predict()
    return ret


def inform(obj):
    post_json(const.MAIL_URL, obj)
