# https://taiga.jpbiz.work:4083/project/tan-nn/us/16

import math, os, sys
# sys.path.append('/usr/lib/python3.7')
# sys.path.insert(0, "/usr/lib/python3.7")
from pathlib import Path

issue_path = Path('.') # cgi-bin call
sys.path.remove(issue_path)

from gym_trading.envs.Trend_env_train import TrendEnvTrain
from util.rd_conf import load_conf

import gym
from stable_baselines.common import set_global_seeds
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import PPO2
from stable_baselines.bench import Monitor

from entity.action_space import ActionSpace

# Config 読み込み
conf = load_conf('./cgi-bin/conf/pqsConfig.json')
max_len = conf['stackMax']
spread = conf['spread']

move_idx = 0
prm = {'window_size': 10,  # window_size 参照すべき直前のデータ数
       'start_idx': 50,  # start_idx 学習データの開始位置
       'end_idx': 100,  # end_idx 学習データの終了位置
       'move_idx': move_idx  # 学習データからの移動分。移動したものを検証データとする。
       }

# ログフォルダの作成
log_dir = './cgi-bin/logs/'
os.makedirs(log_dir, exist_ok=True)


def train():
    # 環境を生成（frame_boundはデータセット内の訓練範囲を開始行数と終了行数で指定）
    env = gym.make('cgi-jp2251-5m-v0', max_len=max_len, spread=spread)
    env = Monitor(env, log_dir, allow_early_resets=True)

    # ベクトル化環境の生成
    env = DummyVecEnv([lambda: env])

    # シードの指定
    set_global_seeds(0)
    env.seed(0)

    # モデルの生成
    model = PPO2('MlpPolicy', env, verbose=1)

    # モデルの学習
    model.learn(total_timesteps=127)

    # モデルの保存
    model.save('trading_model')


def predict():
    ret_act = None
    # モデルの読み込み(学習済みデータがある場合)
    model = PPO2.load('trading_model')

    # モデルのテスト
    env = gym.make('cgi-jp2251-5m-v0', max_len=max_len, spread=spread)
    # env = Monitor(env, log_dir, allow_early_resets=True)

    # シードの指定
    set_global_seeds(0)
    env.seed(0)
    state = env.reset()

    while True:
        # 行動の取得
        action, _ = model.predict(state)
        # 1ステップ実行
        state, reward, done, info = env.step(action)
        # エピソード完了
        if done:
            # print('info:', info)
            ret_act = action
            break

    # グラフのプロット
    '''
    plt.cla()
    env.render_all()
    plt.show()
    '''
    return action.item() - math.floor(len(ActionSpace)/2)
