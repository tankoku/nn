#

from gym_trading.envs.Trend_env_train import TrendEnvTrain
from util.rd_conf import load_conf

import gym
import matplotlib.pyplot as plt
from pathlib import Path
import os
from stable_baselines.common import set_global_seeds

from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import PPO2
from stable_baselines.bench import Monitor

# Config 読み込み
conf = load_conf('./conf/pqsConfig.json')
max_len = conf['stackMax']
spread = conf['spread']

move_idx = 0
prm = {'window_size': 10,  # window_size 参照すべき直前のデータ数
       'start_idx': 50,  # start_idx 学習データの開始位置
       'end_idx': 100,  # end_idx 学習データの終了位置
       'move_idx': move_idx  # 学習データからの移動分。移動したものを検証データとする。
       }

# ログフォルダの作成
log_dir = './logs/'
os.makedirs(log_dir, exist_ok=True)

# 環境を生成（frame_boundはデータセット内の訓練範囲を開始行数と終了行数で指定）
env = gym.make('jp2251-5m-v0', max_len=max_len, spread=spread)
env = Monitor(env, log_dir, allow_early_resets=True)

# シードの指定
set_global_seeds(0)
env.seed(0)

# ベクトル化環境の生成
env = DummyVecEnv([lambda: env])

# モデルの生成
model = PPO2('MlpPolicy', env, verbose=1, ent_coef=0.005, tensorboard_log=log_dir)  # tensorboard --logdir=./logs/.
# model = PPO2('MlpLstmPolicy', env, ent_coef=0.005, nminibatches=1)
# del model  # ロードデモのために一旦トレーニング済みエージェントを削除

# モデルの読み込み(学習済みデータがある場合)
# model = PPO2.load('trading_model')

# モデルの学習
model.learn(total_timesteps=512)

# モデルの保存
model.save('trading_model')

# モデルのテスト
env = gym.make('jp2251-5m-v0', max_len=max_len, spread=spread)
env.seed(0)
state = env.reset()
while True:
    # 行動の取得
    action, _ = model.predict(state)
    # 1ステップ実行
    state, reward, done, info = env.step(action)
    # エピソード完了
    if done:
        print('info:', info)
        break

# グラフのプロット
plt.cla()
env.render_all()
plt.show()
