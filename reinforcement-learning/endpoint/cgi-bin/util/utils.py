# https://taiga.jpbiz.work:4083/project/tan-nn/us/12

import datetime
import json
import math
import numpy as np
import time
import requests

from . import const

const.YEAR_DAYS = 365
const.DAY_TS = 24 * 3600


def action2num(a):
    ret = 0
    if a != 0:
        ret = math.pow(2, (abs(a) - 1))
    return ret


def cal_sharpe_ratio(pl_lst, e=-1, period=30):  # 30days = 30x24x3600
    """
    pl_lst: lost-profit list
    e: end timestamp
    period: days before end
    """
    if e < 0:
        e = time.time()

    s = e - period * const.DAY_TS
    ret_list = list(map(lambda x: x['pl'], filter(lambda x: s < x['ts'] <= e, pl_lst)))

    m = np.mean(ret_list)
    s = np.std(ret_list)
    if 0 == s:
        s = 100
    return np.sqrt(const.YEAR_DAYS / period) * (m / s)
    # np.sqrt(126) * (y.mean() / y.std()) # 21 days per month X 6 months = 126


def datetime_fmt2ts(d, t, sp='.'):
    # 2021.04.21 4:57
    element = datetime.datetime.strptime(d + ' ' + t, "%Y" + sp + "%m" + sp + "%d %H:%M")
    return int(time.mktime(element.timetuple()))


def ts2date(ts, sp='.'):
    return datetime.datetime.fromtimestamp(ts).strftime("%Y" + sp + "%m" + sp + "%d")


def ts2time(ts):
    return datetime.datetime.fromtimestamp(ts).strftime("%H:%M")


def post_json(u, j):
    uu = 'http://katokunou.com/ifis/' + u + '.php'
    r_post = requests.post(uu, json.dumps(j, ensure_ascii=False, indent=1).encode('utf-8'),
                           headers={'Content-Type': 'application/json; charset=UTF-8'})
