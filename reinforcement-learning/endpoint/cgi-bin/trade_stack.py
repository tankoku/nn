# https://taiga.jpbiz.work:4083/project/tan-nn/us/11

from entity.position_node import PositionNode
from entity.position_type import PositionType
from collections import deque


def sum_num(lst):
    return sum(i.num for i in lst)


def sum_val(lst):
    return sum(i.num * i.price for i in lst)


def cal_pl(lst, bid, ask):  # bid = ask - spread
    if not lst:
        return 0

    l = sum_num(lst)
    val = sum_val(lst)

    if lst[-1].position_type == PositionType.LONG:
        return l * bid - val
    else:
        return val - l * ask


class TradeStack:
    def __init__(self, max_len=10, spread=6):
        self.__max = max_len
        self.__stack = deque([])  # not maxlen=max_len
        self.__spread = spread

    def peek(self):
        if self.__stack:  # not empty
            return self.__stack[-1]

        return None

    def stack_type(self):
        if self.peek() is None:
            return PositionType.EMPTY

        return self.peek().position_type

    def stack_len(self):
        if self.__stack:  # not empty
            return sum_num(self.__stack), self.stack_type()

        return 0, PositionType.EMPTY

    def stack_value_diff(self, ask):  # bid = ask - spread
        if self.stack_type() == PositionType.EMPTY:
            return 0
        else:
            return cal_pl(self.__stack, ask - self.__spread, ask)

    def stack_value(self):
        if self.__stack:  # not empty
            return sum_val(self.__stack)

        return 0

    def stack_trade(self, node, ask):  # bid = ask - spread
        """
        Return:
            code: (0) - no close, (-1) - space not enough, (1) - close
            space:
            type:
            pl (profit-loss):
        """
        # current length and stack type
        l, t = self.stack_len()

        # STAY
        if node is None:
            return 0, self.__max - l, t, 0

        in_num = node.num
        in_type = node.position_type

        # purely push
        if t == in_type or t == PositionType.EMPTY:
            if self.__max - l >= in_num:  # enough space
                self.__stack.append(node)
                l, t = self.stack_len()
                return 0, self.__max - l, t, 0
            else:
                return -1, self.__max - l, t, 0

        # pop or pop then push (close)
        # if t != in_type and t != PositionType.EMPTY:
        lst = []

        # only pop
        if l == in_num:  # pop all as close
            while self.__stack:  # not empty
                lst.append(self.__stack.pop())
        elif l > in_num:  # partly pop
            tn = self.__stack.pop()  # top node
            remain = in_num
            while remain >= tn.num:
                lst.append(tn)  # pop top and close
                remain = remain - tn.num
                if remain > 0:
                    tn = self.__stack.pop()  # next top node

            if remain > 0:
                push_num = tn.num - remain
                tn.num = remain
                lst.append(tn)  # to close

                rn = PositionNode(tn.date, tn.time, tn.price, tn.position_type.value)  # re-push node
                rn.num = push_num
                self.__stack.append(rn)  # current position

        # pop then push the remain
        else:
            while self.__stack:  # not empty, pop all
                lst.append(self.__stack.pop())

            # push the remain
            node.num = in_num - l
            self.__stack.append(node)

        l, t = self.stack_len()
        return 1, self.__max - l, t, cal_pl(lst, ask - self.__spread, ask)

    def stack_close_all(self, ask):  # bid = ask - spread
        lst = []
        cd = 0
        if PositionType.EMPTY != self.stack_type():
            cd = 1

        while self.__stack:  # not empty, pop all
            lst.append(self.__stack.pop())

        l, t = self.stack_len()
        return cd, self.__max - l, t, cal_pl(lst, ask - self.__spread, ask)

    def stack_reset(self):
        self.__stack = deque([])  # not maxlen=max_len

    def stack_print(self, ask=0):
        if self.stack_type() != PositionType.EMPTY:
            print('-------------')
            for i in self.__stack:
                print(vars(i))

            l, t = self.stack_len()
            print('-------------')
            print(str(t) + ' | ' + str(l))
            print(str(self.stack_value() / l) + ' | ' + str(self.stack_value_diff(ask)))
        else:
            print('----NANA-----')
