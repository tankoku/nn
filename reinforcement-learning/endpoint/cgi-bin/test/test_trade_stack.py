import unittest

from entity.position_node import PositionNode
from entity.position_type import PositionType
from trade_stack import TradeStack


class MyTestCase(unittest.TestCase):
    __stack = TradeStack(10, 2)
    __ask = 5
    # __bid = 5 - 2
    __spn1 = PositionNode('2021.4.29', '11:11', 1, -1)  # 1
    __spn2 = PositionNode('2021.4.29', '11:22', 2, -2)  # 2
    __spn3 = PositionNode('2021.4.29', '11:33', 3, -3)  # 4
    __spn4 = PositionNode('2021.4.29', '11:44', 4, -4)  # 8
    __bpn1 = PositionNode('2021.4.29', '11:11', 1, 1)  # 1
    __bpn2 = PositionNode('2021.4.29', '12:22', 2, 2)  # 2
    __bpn3 = PositionNode('2021.4.29', '13:33', 3, 3)  # 4
    __bpn4 = PositionNode('2021.4.29', '14:44', 4, 4)  # 8

    def setUp(self):
        pass

    def tearDown(self):
        # pass
        self.__stack.stack_close_all(self.__ask)

    def test_stack_funcs(self):
        self.assertEqual(PositionType.EMPTY, self.__stack.stack_type())

        self.__stack.stack_trade(self.__spn1, self.__ask)
        self.__stack.stack_trade(self.__spn3, self.__ask)
        l, t = self.__stack.stack_len()
        self.assertEqual(5, l)
        self.assertEqual(PositionType.SHORT, t)
        self.assertEqual(-12, self.__stack.stack_value_diff(self.__ask))

        c, m, t, pl = self.__stack.stack_close_all(self.__ask)
        self.assertEqual(1, c)  # code
        self.assertEqual(10, m)  # max
        self.assertEqual(PositionType.EMPTY, t)  # type
        self.assertEqual(-12, pl)  # profit-loss

    def test_stack_trade1(self):
        c, s, t, pl = self.__stack.stack_trade(self.__bpn4, self.__ask)
        self.assertEqual(0, c)  # code
        self.assertEqual(2, s)  # space
        self.assertEqual(PositionType.LONG, t)  # type
        self.assertEqual(0, pl)  # profit-loss

        c, s, t, pl = self.__stack.stack_trade(self.__bpn1, self.__ask)
        self.assertEqual(0, c)  # code
        self.assertEqual(1, s)  # space
        self.assertEqual(PositionType.LONG, t)  # type
        self.assertEqual(0, pl)  # profit-loss

        c, s, t, pl = self.__stack.stack_trade(self.__bpn2, self.__ask)
        self.assertEqual(-1, c)  # code
        self.assertEqual(1, s)  # space
        self.assertEqual(PositionType.LONG, t)  # type
        self.assertEqual(0, pl)  # profit-loss
        # self.__stack.stack_print()

    def test_stack_trade2(self):
        self.__stack.stack_trade(self.__bpn1, self.__ask)
        self.__stack.stack_trade(self.__bpn1, self.__ask)
        self.__stack.stack_trade(self.__bpn2, self.__ask)
        self.__stack.stack_trade(self.__bpn2, self.__ask)

        c, s, t, pl = self.__stack.stack_trade(self.__spn2, self.__ask)
        self.assertEqual(1, c)  # code
        self.assertEqual(6, s)  # space
        self.assertEqual(PositionType.LONG, t)  # type
        self.assertEqual(2, pl)  # profit-loss
        # self.__stack.stack_print()

    def test_stack_trade3(self):
        self.__stack.stack_trade(self.__spn1, self.__ask)
        self.__stack.stack_trade(self.__spn1, self.__ask)
        self.__stack.stack_trade(self.__spn1, self.__ask)
        self.__stack.stack_trade(self.__spn2, self.__ask)
        # self.__stack.stack_print()

        c, s, t, pl = self.__stack.stack_trade(self.__bpn3, self.__ask)
        self.assertEqual(1, c)  # code
        self.assertEqual(9, s)  # space
        self.assertEqual(PositionType.SHORT, t)  # type
        self.assertEqual(-14, pl)  # profit-loss

        c, s, t, pl = self.__stack.stack_trade(self.__bpn4, self.__ask)
        self.assertEqual(1, c)  # code
        self.assertEqual(3, s)  # space
        self.assertEqual(PositionType.LONG, t)  # type
        self.assertEqual(-4, pl)  # profit-loss
        # self.__stack.stack_print()
        self.assertEqual(-7, self.__stack.stack_value_diff(self.__ask))


if __name__ == '__main__':
    unittest.main()
