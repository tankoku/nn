import unittest
# import os, sys

# sys.path.append(os.path.abspath(".."))
from util.utils import *


class MyTestCase(unittest.TestCase):

    def test_action2num(self):
        self.assertEqual(4, action2num(-3))

    def test_cal_sharpe_ratio(self):
        ts = time.time()
        pl_lst = [
            {
                "ts": 0,
                "pl": 45
            },
            {
                "ts": ts - 10,
                "pl": 3
            },
            {
                "ts": ts - 8,
                "pl": -1
            },
            {
                "ts": ts - 6,
                "pl": 9
            },
            {
                "ts": ts - 4,
                "pl": 2
            }
        ]
        self.assertEqual(6.46248483351612, cal_sharpe_ratio(pl_lst, period=7))  # 1week
        self.assertEqual(3.1121225903784473, cal_sharpe_ratio(pl_lst, ts - 5))  # 1month

    def test_datetime_fmt2ts(self):
        self.assertEqual(1619688720, datetime_fmt2ts('2021.04.29', '18:32'))
        self.assertEqual(1619688840, datetime_fmt2ts('2021/04/29', '18:34', '/'))


if __name__ == '__main__':
    unittest.main()
