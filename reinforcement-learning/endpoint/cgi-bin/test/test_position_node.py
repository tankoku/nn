import unittest


from entity.position_node import PositionNode
from entity.position_type import PositionType


class MyTestCase(unittest.TestCase):
    __expect_price = 28536.15
    __expect_timestamp = 1619662920.0
    __expect_position_type = PositionType.SHORT
    __expect_num = 4  # 2^(abs(-3)-1)

    __pn = PositionNode('2021.4.29', '11:22', __expect_price, -3)

    def test_price(self):
        self.assertEqual(self.__expect_price, self.__pn.price)

    def test_timestamp(self):
        self.assertEqual(self.__expect_timestamp, self.__pn.timestamp)

    def test_position_type(self):
        self.assertEqual(self.__expect_position_type, self.__pn.position_type)

    def test_num(self):
        self.assertEqual(self.__expect_num, self.__pn.num)


if __name__ == '__main__':
    unittest.main()
