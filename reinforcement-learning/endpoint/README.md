
### Prerequisite:
Python3

### Process
1. Boot up:
    * Test mode:
        ``` bash
        $ python3 -m http.server 8081 --cgi
        ```
    * Product mode:
        ``` bash
        $ setsid python3 -m http.server 8081 --cgi > /dev/null 2>&1
        ```
1. Test:
    * run test_client.sh
        ``` bash
        $ chmod +x test_client.sh
        $ ./test_client.sh
        ```
1. Endpoint:
    * zigui.py:
        ``` bash
        $ chmod +x zigui.py
        ```
    * http://wxt.jpbiz.work:8081/cgi-bin/zigui.py

### Raspberry tensorflow 1.15
https://github.com/PINTO0309/Tensorflow-bin
```
$ pip3 uninstall tensorflow
$ wget "https://raw.githubusercontent.com/PINTO0309/Tensorflow-bin/master/tensorflow-1.15.0-cp37-cp37m-linux_armv7l_download.sh"
$ ./tensorflow-1.15.0-cp37-cp37m-linux_armv7l_download.sh
$ pip3 install tensorflow-1.15.0-cp37-cp37m-linux_armv7l.whl
```