# Model Building Requirements (< Python 3.7.9)
numpy==1.16.5
pandas==1.0.3
stockstats
scikit-learn==0.21.1
gym==0.15.3
gym-anytrading==1.2.0
requests==2.25.1
# stable-baselines[mpi]
stable-baselines
tensorflow==1.15.4

joblib==0.15.1

# plot
matplotlib==3.2.1

# testing requirements
pytest>=5.3.2,<6.0.0

# packaging
setuptools>=41.4.0,<42.0.0
wheel>=0.33.6,<0.34.0

# sudo apt-get update && sudo apt-get install cmake libopenmpi-dev python3-dev zlib1g-dev libgl1-mesa-glx


