#!/usr/bin/python3
# coding: utf-8

from __future__ import print_function

import codecs
import datetime
import json
import re
import sys
import csv
import os
import time
import random
import requests
import urllib3

from urllib3.exceptions import InsecureRequestWarning
urllib3.disable_warnings(InsecureRequestWarning)

from collections import deque
from bs4 import BeautifulSoup
from math import fabs
from time import sleep

BASE_URL    = 'https://jp.investing.com/indices'
MAIN_URI    = '/indices-futures'
CURR_FILE   = '/tmp/future.json'

JP_225_FUTURES    = 'japan-225-futures'
JP_TOPIX_FUTURES  = 'topix-futures'

JP_TARGETS = {
  JP_225_FUTURES: {
    'nm': '日経225先物',
    'uri': JP_225_FUTURES,
    'chg': 0.00251
  },
  JP_TOPIX_FUTURES: {
    'nm': 'TOPIX先物',
    'uri': JP_TOPIX_FUTURES,
    'chg': 0.00301
  }
}


# currently want to be warned in 9min (3min * 3)
OLD_CNT     = 3
CHANGE_RATE = 0.001
SCALE       = 1.08

def postJson(u,j):
  uu = 'http://katokunou.com/ifis/' + u + '.php'
  r_post = requests.post(uu, json.dumps(j, ensure_ascii=False, indent=1).encode('utf-8'), headers={'Content-Type': 'application/json; charset=UTF-8'})


def splitStock(s):
  nmcd = s.split("(")
  cd = re.sub('[)]', '', nmcd[1])
  return nmcd[0], cd


def writeJsonData(path, jsonObj):
   # write all traders
   # [x.encode('utf-8') for x in traders]
   with codecs.open(path,'w','utf-8') as f:
      f.write(json.dumps(jsonObj, ensure_ascii=False, indent=2, sort_keys=False, separators=(',', ': ')))


def loadJsonData(path, arrFlg=False):
   jsonData = {}
   arrData = []

   if os.path.exists(path):
      # Relative Path
      with open(path, 'r', encoding='utf-8') as infile:
         if arrFlg:
            arrData = json.load(infile)
         else:
            jsonData = json.load(infile)

   if arrFlg:
      return arrData
   else:
      return jsonData


def cnvtFloat(d):
  return '%.2f' % (d*1000)


def toWarn(t, c, o):
  rtn = None
  cnt = range(len(o[t]))
  tgtUris = [v['uri'] for v in US_TARGETS.values()]
  idx = tgtUris.index(t)

  nms = [v['nm'] for v in US_TARGETS.values()]

  # different targets to be monitored with different changed rates
  chgs = [v['chg'] for v in US_TARGETS.values()]

  changed = [c[t] / o[t][i] - 1.0 for i in cnt]
  # print(",".join(map(cnvtFloat, changed)))
  if any([(fabs(ch) > chgs[idx]) for ch in changed]) and \
     fabs(sum(changed)) > chgs[idx]*SCALE:
    rtn = nms[idx] + " (" + ",".join(map(cnvtFloat, changed)) +")‰"

  return rtn

now = datetime.datetime.now() # "%Y%m%d%H%M%S"

try:

  # out of service
  if (now.weekday() == 0) and (now.hour < 7) or \
     (now.weekday() == 5) and (now.hour > 7) or \
     (now.weekday() == 6):
    exit()

  sleep(int(random.uniform(30, 150))) # every 3min + sleep(s)
  now = datetime.datetime.now()

  json_list = []
  json_data = {}

  # old_list_data = loadJsonData(CURR_FILE)
  # new_list_data = {}

  headers = requests.utils.default_headers()
  headers.update({
      'Host': 'jp.investing.com',
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
      'Accept-Language': 'ja,en;q=0.9,en-GB;q=0.8,en-US;q=0.7',
      'Accept-Encoding': 'gzip, deflate, br',
#     'Cookie': '_ga=GA1.3.136370371.1475894645; USER_ID=unknown; CHECKED_LIST=chk_price+chk_investor; STOCK_LIST=%25E3%2583%2588%25E3%2583%25A8%25E3%2582%25BF%25E8%2587%25AA%25E5%258B%2595%25E8%25BB%258A%2520%287203%29%252B%25E6%2597%25A5%25E7%2594%25A3%25E8%2587%25AA%25E5%258B%2595%25E8%25BB%258A%2520%287201%29%252B%25E6%259C%25AC%25E7%2594%25B0%25E6%258A%2580%25E7%25A0%2594%25E5%25B7%25A5%25E6%25A5%25AD%2520%287267%29%252B%25E3%2583%259E%25E3%2583%2584%25E3%2583%2580%2520%287261%29; TABLE_ORDER=0%3Acomp_price1%3Acomp_investor2%3Acomp_factor_act3%3Acomp_q_factor_act4%3Acomp_factor_est5%3Acomp_analyst_score6%3Acomp_cf7%3Acomp_factor_prog8%3Acomp_segment_sales9%3Acomp_segment_prof10%3Acomp_pl11%3Acomp_bs; _gid=GA1.3.1890570330.1597296210; C_HC=40879a952cfaccbf8c5805f07ae7669043bfe3cf; _gat=1; AWSELB=23E1FBA2A34066D8423A64E59E8767DE12E3177863341232CC2F1A1F726527642BBEBF7E088DDC93E7ED316AFE15D438102BF8C680BE73BFA1868DC3DE0FE68D05D7B501; AWSELBCORS=23E1FBA2A34066D8423A64E59E8767DE12E3177863341232CC2F1A1F726527642BBEBF7E088DDC93E7ED316AFE15D438102BF8C680BE73BFA1868DC3DE0FE68D05D7B501',
      'Connection': 'keep-alive',
      'Upgrade-Insecure-Requests': '1',
      'Cache-Control': 'max-age=0',
      'Sec-Fetch-Site': 'none',
      'Sec-Fetch-Mode': 'navigate',
      'Sec-Fetch-User': '?1',
      'Sec-Fetch-Dest': 'document',
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 Edg/85.0.564.68'
  })


  r = requests.get(BASE_URL+MAIN_URI, headers=headers, verify=False)  #requestsを使って、webから取得
  soup = BeautifulSoup(r.text, 'lxml')                  #要素を抽出 (lxml)

  tm = datetime.datetime.now()
  ts = int(tm.timestamp())
  today = tm.strftime("%Y/%m/%d")

  # リアルタイム先物相場（CFD取引）ストリーミング
  tbl = soup.find('table', class_='genTbl closedTbl elpTbl elp20 crossRatesTbl')
  # print("debug: " + tbl.text)
  trs = tbl.find_all('tr')

  curr_dict = {}

  # tr in the 1st table
  for tr in trs:
    target = []
    tds = tr.find_all('td')

    # each Not in targets
    targets = [v['nm'] for v in JP_TARGETS.values()]
    if not any([td.text in targets for td in tds]):
      continue

    '''
    for i in range(1, 8):
      target.append(tds[i].text)
    '''

    target.append('指数: ' + tds[1].text)
    target.append('月: ' + tds[2].text)
    target.append('現在値: ' + tds[3].text)
    target.append('高値: ' + tds[4].text)
    target.append('安値: ' + tds[5].text)
    target.append('前日比: ' + tds[6].text)
    target.append('変動(%): ' + tds[7].text)
    target.append('時間: ' + tds[8].text)

    idx = targets.index(tds[1].text)
    tgtUris = [v['uri'] for v in JP_TARGETS.values()]
    target.append('url: ' + BASE_URL + '/' + tgtUris[idx])

    # print("debug: " + ','.join(targets))

    json_list.append(target)

    tgtNm = tgtUris[idx]    # .split("/")[-1]
    tgtPc = float(re.sub('[,]', '', tds[3].text))

    curr_dict[tgtNm] = tgtPc

    with open("./data/" + tgtNm + ".csv" , "a") as myfile:
      myfile.write("{}\n".format(','.join([tm.strftime("%Y%m%d%H%M"), str(tgtPc)])))

  '''
    # created new list to be saved
    d = deque(old_list_data[tgtNm], OLD_CNT)
    d.append(tgtPc)
    new_list_data[tgtNm] = list(d)

  warnInfoArr = list(map(lambda t: toWarn(t, curr_dict, old_list_data), old_list_data.keys()))
  # print(warnInfoArr)
  if any([None != m for m in warnInfoArr]):
    json_data["TITLE"] = 'Happening ±(0.25~0.40)% in 3x 4MIN in: ' + ",".join(map(str, warnInfoArr))
    json_data["TIME"] = now.strftime('%Y-%m-%d %H:%M:%S')
    json_data["URL"] = BASE_URL+MAIN_URI
    json_data["WARN"] = json_list

    # print(json_data)

    # debug
    # json_str = json.dumps(json_data, ensure_ascii=False, indent=1).encode('utf-8')
    # print(json_str)

    url_items = 'mp_publisher'
    postJson(url_items, json_data)

  # update after sent mail
  writeJsonData(CURR_FILE, new_list_data)
  '''

except Exception as e:
    print("error: {0}".format(e), file=sys.stderr)
    exitCode = 2

