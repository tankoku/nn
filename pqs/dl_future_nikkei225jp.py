#!/usr/bin/python3
# coding: utf-8

from __future__ import print_function

import codecs
from datetime import datetime, time
import json
import re
import sys
import csv
import os
import random

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
# import chromedriver_binary
import requests
import urllib3

from urllib3.exceptions import InsecureRequestWarning

urllib3.disable_warnings(InsecureRequestWarning)

from collections import deque
from bs4 import BeautifulSoup
from math import fabs
from time import sleep

JP_225_FUTURES = 'japan-225-futures'
JP_TOPIX_FUTURES = 'topix-futures'
BASE_URL = 'https://nikkei225jp.com/cme/index.php'
HEADERS = {
    ':authority': 'nikkei225jp.com',
    ':method': 'GET',
    ':path': '/cme/index.php',
    ':scheme': 'https',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'ja,en;q=0.9,en-GB;q=0.8,en-US;q=0.7',
    'cache-control': 'max-age=0',
    'cookie': '@stockmeta_nkkei_com=true; _ga=GA1.2.804864291.1600061668; _gid=GA1.2.1107235118.1608859414; @NK225_home=2_0_0_0_0_; __gads=ID=7e2b10ad4a41d5f4:T=1608865424:S=ALNI_MZMaHGous6-Xa690zfm2dEKUBFFow',
    'sec-fetch-dest': 'document',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-site': 'none',
    'sec-fetch-user': '?1',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'
}


def is_time_between(begin_time, end_time, check_time=None):
    # If check time is not given, default to current UTC time
    check_time = check_time or datetime.utcnow().time()
    if begin_time < end_time:
        return check_time >= begin_time and check_time <= end_time
    else:  # crosses midnight
        return check_time >= begin_time or check_time <= end_time


if __name__ == "__main__":
    now = datetime.now()  # "%Y%m%d%H%M%S"
    wd = now.weekday()
    ct = now.time()

    # out of service
    if not ( \
                    ((0 < wd < 5) and (
                            is_time_between(time(8, 45), time(15, 10), ct) or is_time_between(time(16, 30), time(5, 25),
                                                                                              ct))) or
                    ((wd == 0) and (is_time_between(time(8, 45), time(15, 10), ct) or is_time_between(time(16, 30),
                                                                                                      time(23, 55),
                                                                                                      ct))) or
                    ((wd == 5) and (is_time_between(time(0, 0), time(5, 25), ct))) \
            ):
        exit()

    # sleep(int(random.uniform(15, 105)))  # every 4min + sleep(s)

    options = Options()
    # options = webdriver.ChromeOptions()

    '''
  wget https://dl.google.com/linux/linux_signing_key.pub
  sudo apt-key add linux_signing_key.pub
  echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list
  sudo apt-get update
  sudo apt-get install google-chrome-stable
  google-chrome --version

  sudo apt-get install chromium-chromedriver
  sudo dpkg -i chromium-chromedriver_61.0.3163.79-0ubuntu0.14.04.1196_armhf.deb
  ls -l /usr/lib/chromium-browser/chromedriver
  '''
    # options.binary_location = '/usr/bin/chromium-browser'
    options.add_argument('--headless')
    # options.add_argument('--lang=ja-JP')
    # options.add_argument('--window-size=1280,1024')

    browser = webdriver.Chrome(options=options)  # ブラウザを操作するオブジェクトを生成
    # browser = webdriver.Chrome(chrome_options=options)

    browser.implicitly_wait(5)
    # Set the request header using the `header_overrides` attribute
    browser.header_overrides = HEADERS

    try:
        start = datetime.now()

        browser.get(BASE_URL)  # URLへアクセス
        soup = BeautifulSoup(browser.page_source, 'html.parser')  # 要素を抽出 (lxml)

        tm = datetime.now()
        ts = int(tm.timestamp())
        today = tm.strftime("%Y%m%d")  # ("%Y/%m/%d")

        # 日経平均先物 大取ミニ <td class="val eq" id=V136>
        V136 = soup.select('#V136')
        # V136 = soup.find("td", {"id": "V136"})
        # print(V136[0].text)

        with open("./data/" + JP_225_FUTURES + '_' + today + ".csv", "a") as myfile:
            myfile.write(
                "{}\n".format(','.join([tm.strftime("%Y%m%d%H%M"), str(float(re.sub('[,]', '', V136[0].text)))])))

    except Exception as e:
        print("error: {0}".format(e), file=sys.stderr)
        exitCode = 2

    finally:
        browser.quit()
        # print("Finished: {}".format(datetime.now() - start))
