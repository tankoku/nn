
struct band_node {
  double val;
  string nm;
};


for (period in (current, M5, H1, D1, W1)) {
  for (sigma in (-3σ, ... avg, ... 3σ)) {
    struct band_node n = new band_node();
    n.val = iBands(JP225,PERIOD_W1,25,1,0,PRICE_CLOSE,MODE_LOWER,0)

    string mark = PERIOD_CURRENT == period? "★ ": "-- ";
    n.nm = mark + num2str(n.val) + "("+period + sigma +")\r\n";
  }
}

string subject = "notification";
string txt = " \
-- 28980 (W1 1σ) \r\n \ 
-- 28865 (D1 2σ) \r\n \
-- 28750 (H1 1σ) \
-- 28552 (M5 Avg) \
★ 28475 (current) \
-- 28452 (M5 -1 1σ) \
-- 28444 (D1 1σ) \
-- 28375 (H1 Avg) \
-- 28180 (W1 Avg) ";
SendMail(subject, txt);

１σ＝68.26％
２σ＝95.44％
３σ＝99.74％