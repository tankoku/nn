//+------------------------------------------------------------------+
//|                                                     AutoMail.mq4 |
//|                        Copyright 2021, MetaQuotes Software Corp. |
//|                                             https://www.mql4.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2021, MetaQuotes Software Corp."
#property link      "https://www.mql4.com"
#property version   "1.00"
#property strict

#include <ZiGuiLib\RakutenSym.mqh>
#include <ZiGuiLib\Deviation.mqh>
#include <ZiGuiLib\BandNode.mqh>
#include <ZiGuiLib\PeriodSym.mqh>
#include <ZiGuiLib\Utility.mqh>


input int iband_period = 25;  // ibandaveraging period
input int imacd_period = PERIOD_H1; // imacd period
input int imacd_fast = 12;    // Fast EMA averaging period
input int imacd_slow = 26;    // Slow EMA averaging period
input int imacd_signal = 9;   // Signal line averaging period
input double imacd_trend = 0.6; // threshold
input double ratios_sigma = 1.7; // Sigma
input int ratios_len = 200;   // n days
input int clz_days = 5;       // au/ag close n days
input int auto_tp = 1500;     // auto take profits

#define TARGETS   (SYM_LAST)
#define SHIFTS    (4)


// internal datatype
struct AuAgRatio {
  string txt;
  bool flg;
};

// global variables
const int Magic = 20211224;

Utility*  util;
BandNode* bns[TARGETS][W1][DEV_LAST];
BandNode* currentBn[TARGETS];
BandNode* sortedBns[TARGETS][W1*2 + 1]; // + current

AuAgRatio aar = {"", FALSE};

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
  util = new Utility();
  // initialize all band nodes
  for (RakutenSym i = USDJPY; i < TARGETS; i++) {
    currentBn[i] = new BandNode("(**NOW)");
    for (PeriodSym j = M5; j < W1; j++)
      for (Deviation k = DEV_M3; k < DEV_LAST; k++)
        // string deviation = k == DEV_LAST? "Mean": DevStr[k];
        bns[i][j][k] = new BandNode(StringConcatenate("(", PeriodStr[j], "  ", DevStr[k], ")"));
  }
//---
  return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
  delete util;
  for (RakutenSym i = USDJPY; i < TARGETS; i++) {
    delete currentBn[i];
    for (PeriodSym j = M5; j < W1; j++)
      for (Deviation k = DEV_M3; k < DEV_LAST; k++)
        delete bns[i][j][k];
  }

  }

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
// void start()
int start() // start関数は、適用したチャートの通貨ペアのレートが配信されるタイミングで動作します。
  {
    // close gold and silver strategy
    /* take profits: profits sum for same comments pair >= 2000 -> close */
    if (MarketInfo(RakutenSymStr[XAUUSD], MODE_TRADEALLOWED) == 1 &&
        MarketInfo(RakutenSymStr[XAGUSD], MODE_TRADEALLOWED) == 1) // Trade is allowed for the symbol
      for (int i = 0; i < OrdersTotal(); i++) {
        // gold
        int auTickt = 0;
        double auProfit = 0;
  
        // silver
        int agTickt = 0;
        double agProfit = 0;
        if (OrderSelect(i, SELECT_BY_POS, MODE_TRADES) &&
          OrderSymbol() == RakutenSymStr[XAUUSD]) {
          string auCmnt = OrderComment(); // gold
          auTickt = OrderTicket();
          auProfit = OrderProfit();
  
          // silver
          for (int j = 0; j < OrdersTotal(); j++) {
            if (OrderSelect(j, SELECT_BY_POS, MODE_TRADES) &&
              OrderSymbol() == RakutenSymStr[XAGUSD] &&
              auCmnt == OrderComment()) { // silver pair
              agTickt = OrderTicket();
              agProfit = OrderProfit();
              break;
            }
          }
        }
        if (auProfit + agProfit > auto_tp) {
          int retry = 10; // prevent deadlock
          OrderSelect(auTickt, SELECT_BY_TICKET);
          while(!OrderClose(OrderTicket(),OrderLots(),0,0,clrYellow) && retry-- > 0)
            Sleep(500);
  
          retry = 10;
  
          OrderSelect(agTickt, SELECT_BY_TICKET);
          while(!OrderClose(OrderTicket(),OrderLots(),0,0,clrYellow) && retry-- > 0)
            Sleep(500);
        }
      }

    if (!(25 == (int)fmod(Minute(), 30))) // every 20min
      return (0);

    int lens[TARGETS];
    // use USDJPY for debugging
    for (RakutenSym i = USDJPY; i < TARGETS; i++) {
      if (MarketInfo(RakutenSymStr[i], MODE_TRADEALLOWED) != 1) // Trade is allowed for the symbol
        continue;

      for (PeriodSym j = M5; j < W1; j++) {
        for (int k = 4; k > 0; k--) {
          if (k == 1) // mean in middle
            bns[i][j][4-k].setVal(iBands(RakutenSymStr[i],PeriodVal[j],iband_period,k,0,PRICE_WEIGHTED,MODE_MAIN,0));
          else {
            bns[i][j][4-k].setVal(iBands(RakutenSymStr[i],PeriodVal[j],iband_period,k-1,0,PRICE_WEIGHTED,MODE_LOWER,0));
            bns[i][j][k+2].setVal(iBands(RakutenSymStr[i],PeriodVal[j],iband_period,k-1,0,PRICE_WEIGHTED,MODE_UPPER,0));
          }
        }
        /*
        for (Deviation k = DEV_M3; k < DEV_LAST; k++)
          PrintFormat("%s -- %s %f --", RakutenSymStr[i], bns[i][j][k].getName(), bns[i][j][k].getVal());
        */
      }

      lens[i] = addInWanted(i); // and sorted

      // judge imacd or deviation for glod and silver
      // send mail when flg is TRUE
      if (judgeSendMail(i)) {
        string txt = "";
        for (int s = 0; s < lens[i]; s++)
          txt = StringConcatenate(txt, DoubleToString(sortedBns[i][s].getVal(), util.getSymDigits(i)), " ", sortedBns[i][s].getName(), "\r\n");

        txt = StringConcatenate(txt, aar.txt);

        PrintFormat("-- %s", txt);
        SendMail(RakutenSymStr[i] + " " + util.getCurrentStr(i), txt);
      }
    }

    // wait for 62 sec
    Sleep(62000); // milliseconds unit

    // close gold and silver strategy
    /* clear: OrderOpenTime - local > 48H -> close */
    for (int i = 0; i < OrdersTotal(); i++) {
      if (OrderSelect(i, SELECT_BY_POS, MODE_TRADES) &&
        (OrderSymbol() == RakutenSymStr[XAUUSD]|| OrderSymbol() == RakutenSymStr[XAGUSD])) {
        int retry = 10; // prevent deadlock
        if (!util.isTradeOpenInDays(OrderTicket(), clz_days))
          while(!OrderClose(OrderTicket(),OrderLots(),0,0,clrYellow) && retry-- > 0)
            Sleep(500);
      }
    }

    return 0;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Sendmail judgement logic function                                |
//+------------------------------------------------------------------+
bool judgeSendMail(RakutenSym sym)
  {
  bool ret = FALSE;
//---
  double imacdMains[SHIFTS];
  double imacdSigs[SHIFTS];
  bool gradientFlg = FALSE;
  bool crossFlg = FALSE;
  double ratios[];

  ArrayFill(imacdMains, 0, SHIFTS, 0);
  ArrayFill(imacdSigs, 0, SHIFTS, 0);
  aar.flg = FALSE;
  aar.txt = "----------- \r\n";

  // fetch macd info for 4 elements
  for (int i = SHIFTS-1; i >= 0; i--) {
    imacdMains[i] = iMACD(RakutenSymStr[sym],imacd_period,imacd_fast,imacd_slow,imacd_signal,PRICE_WEIGHTED,MODE_MAIN,i);
    imacdSigs[i]  = iMACD(RakutenSymStr[sym],imacd_period,imacd_fast,imacd_slow,imacd_signal,PRICE_WEIGHTED,MODE_SIGNAL,i);
  }

  // trend reverses or relieves
  double diff1 = imacdMains[1] - imacdMains[2];
  double diff0 = imacdMains[0] - imacdMains[1];

  // current trend keeping
  bool currTrendFlg = (imacdMains[2] - imacdMains[3])*diff1 >= 0? TRUE: FALSE;

  gradientFlg = diff1*diff0<=0? TRUE: (fabs(diff0/diff1)<imacd_trend? TRUE: FALSE);
  PrintFormat("%s -- trend macd %f  %f", RakutenSymStr[sym], diff1, diff0);
  if (currTrendFlg && gradientFlg) {
    string tmp = "V->A";
    if (diff1 > 0 || (diff1 == 0 && diff0 < 0))
      tmp = "A->V";
    aar.txt = StringConcatenate(aar.txt, tmp, " MACD(", DoubleToString(imacdMains[0], 2), ") Trend changed!\r\n");
  }

  // main & signal cross
  diff1 = imacdMains[1] - imacdSigs[1];
  diff0 = imacdMains[0] - imacdSigs[0];
  crossFlg = diff1 * diff0 <= 0?
    TRUE: FALSE;
  PrintFormat("%s -- macd vs signals  %f  %f  %f  %f", RakutenSymStr[sym], imacdMains[1], imacdSigs[1], imacdMains[0], imacdSigs[0]);
  if (crossFlg) {
    if (diff1 > 0 || (diff1 == 0 && diff0 < 0))
      aar.txt = StringConcatenate(aar.txt, "MACD VxA Signal Cross!\r\n");
    else
      aar.txt = StringConcatenate(aar.txt, "MACD AxV Signal Cross!\r\n");
  }

  // if gold then goto judge gold/silver ratio
  if (sym == XAUUSD) {
    ArrayResize(ratios, ratios_len, 10);
    //--- initialize the array elements with EMPTY_VALUE=DBL_MAX value
    ArrayInitialize(ratios, EMPTY_VALUE);

    for (int i = ratios_len; i > 0; i--)
      ratios[i-1] = iClose(RakutenSymStr[sym], PERIOD_H1, i-1)/iClose(RakutenSymStr[sym-1], PERIOD_H1, i-1); // PERIOD_D1

    double upper2P = iBandsOnArray(ratios,0,ratios_len,ratios_sigma,0,MODE_UPPER,0); // +2σ
    double lower2M = iBandsOnArray(ratios,0,ratios_len,ratios_sigma,0,MODE_LOWER,0); // -2σ

    double last = util.getCurrent(XAUUSD)/util.getCurrent(XAGUSD);
    string lastStr = DoubleToString(last, 2);
    if (last >= upper2P || last <= lower2M) {
      int xauOp = OP_BUY;
      int xagOp = OP_SELL;
      string tmp = StringConcatenate(aar.txt, lastStr, " : ", DoubleToString(lower2M, 2), " ~ " ,DoubleToString(upper2P, 2));
      
      if (last >= upper2P) {
        xauOp = OP_SELL;  // short gold   (AU)
        xagOp = OP_BUY;   // long  silver (AG)

        tmp = StringConcatenate(aar.txt, DoubleToString(lower2M, 2), " ~ " ,DoubleToString(upper2P, 2), " : ", lastStr);
      }

      const string cmnt = TimeToString(TimeLocal(), TIME_DATE|TIME_MINUTES);
      while(OrderSend(RakutenSymStr[XAUUSD], xauOp, 1, 0,
                    0, 0, 0, cmnt,
                    Magic, 0, clrRed) < 0)
        Sleep(500);
      Sleep(500);
      while(OrderSend(RakutenSymStr[XAGUSD], xagOp, (int)round(last/10), 0,
                    0, 0, 0, cmnt,
                    Magic, 0, clrGreen) < 0)
        Sleep(500);

      aar.txt = tmp;
      aar.flg = TRUE;
    }

    ArrayFree(ratios);
  }

  ret = (currTrendFlg && gradientFlg) || crossFlg;
  return sym != XAUUSD? ret: ret || aar.flg;
  }
//+------------------------------------------------------------------+

/* x, y, z の中間値を返す */
BandNode* med3(BandNode*& x, BandNode*& y, BandNode*& z, int ord) {
  if (((x.getVal() < y.getVal()) && (ord == MODE_ASCEND)) || ((x.getVal() > y.getVal()) && (ord == MODE_DESCEND))) // ASC || DESC
    if (y.getVal() < z.getVal()) return ord == MODE_ASCEND? y: z; else if (z.getVal() < x.getVal()) return ord == MODE_ASCEND? x: z; else return z; else
    if (z.getVal() < y.getVal()) return ord == MODE_ASCEND? y: z; else if (x.getVal() < z.getVal()) return ord == MODE_ASCEND? x: z; else return z;
}

//+------------------------------------------------------------------+
//|ックソート                                                          |
//+------------------------------------------------------------------+
/* クイックソート
 * a     : ソートする配列 array
 * left  : ソートするデータの開始位置
 * right : ソートするデータの終了位置
 */
void quicksort(RakutenSym sym, int left, int right, int ord) {
   if (left < right) {
      int i = left, j = right;
      BandNode* tmp;
      BandNode* pivot = med3(sortedBns[sym][i], sortedBns[sym][i + (j - i) / 2], sortedBns[sym][j], ord); /* (i+j)/2ではオーバーフローしてしまう */
      while (TRUE) { /* a[] を pivot 以上と以下の集まりに分割する */
         if (ord != MODE_DESCEND) {  // ASC
            while (sortedBns[sym][i].getVal()  < pivot.getVal()) i++; /* sortedBns[sym][i] >= pivot となる位置を検索 */
            while (pivot.getVal() < sortedBns[sym][j].getVal())  j--; /* a[j] <= pivot となる位置を検索 */
         } else {         // DESC
            while (sortedBns[sym][i].getVal()  > pivot.getVal()) i++; /* a[i] <= pivot となる位置を検索 */
            while (pivot.getVal() > sortedBns[sym][j].getVal())  j--; /* a[j] >= pivot となる位置を検索 */
         }
         if (i >= j) break;
         tmp = sortedBns[sym][i]; sortedBns[sym][i] = sortedBns[sym][j]; sortedBns[sym][j] = tmp; /* a[i],a[j] を交換 */
         i++; j--;
      }
      quicksort(sym, left, i - 1, ord);  /* 分割した左を再帰的にソート */
      quicksort(sym, j + 1, right, ord); /* 分割した右を再帰的にソート */
   }
}
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Sendmail judgement logic function                                |
//+------------------------------------------------------------------+
int addInWanted(RakutenSym sym) {
  const double current = util.getCurrent(sym);
  int retLen = 0;

  for (PeriodSym j = M5; j < W1; j++) {
    for (Deviation k = DEV_M3; k < DEV_LAST; k++) {
      double tmp = bns[sym][j][k].getVal();
      if ((current <= tmp && k == DEV_M3) || (current >= tmp && k == DEV_P3)) {
        sortedBns[sym][retLen++] = bns[sym][j][k];
        break;
      }
      if (current < tmp) {
        sortedBns[sym][retLen++] = bns[sym][j][k-1];
        sortedBns[sym][retLen++] = bns[sym][j][k];
        break;
      }
    }
  }

  currentBn[sym].setVal(current);
  sortedBns[sym][retLen++] = currentBn[sym];

  // valid length in aa might now might be less than (W1*2+1)
  // sort by iband value
  quicksort(sym, 0, retLen - 1, MODE_DESCEND); // desc by val
  return retLen; // valid length in aa
}
//+------------------------------------------------------------------+

