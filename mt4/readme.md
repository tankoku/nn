
* Taiga reference

1. https://taiga.jpbiz.work:4083/project/tan-nn/task/25
1. https://taiga.jpbiz.work:4083/project/tan-nn/task/26


* 目的

1. 据各自 imacd 交叉及梯度变化条件，配信 JP225 指数及金银价格。
    1. JP225 60min 指标
    1. 金银 1 day or 12 hours
1. 金银按价格比序列计算 60min 周期的±σ， 在偏离２±σ之外时配信， 提供对冲交易参考。
1. 指数及价格
现指数及现价同布林线进行比较，分别抽出距离当前价最近的１±σ，２±σ，３±σ及mean

* 配信周期 per 30min （当前每小时25min， 55min）

* tips
    * NKF
    ```
    $ nkf -g <textdata> confirm the encoding
    $ nkf -w --overwrite <textdata> convert to utf-8
    ```

